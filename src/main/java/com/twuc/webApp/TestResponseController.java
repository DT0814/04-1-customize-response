package com.twuc.webApp;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class TestResponseController {

    @GetMapping("/no-return-value")
    public void testVoidResponse() {

    }

    @GetMapping("/no-return-value-with-annotation")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void testResponseStatus204() {

    }

    @GetMapping("/messages/{message}")
    public String testResponseReturnString(@PathVariable(name = "message") String message) {
        return message;
    }

    @GetMapping("/messages-objects/{message}")
    public Message testResponseReturnObject(@PathVariable(name = "message") String message) {
        return new Message(message);
    }

    @GetMapping("/message-objects-with-annotation/{message}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Message messageObjectsWithAnnotation(@PathVariable(name = "message") String message) {
        return new Message(message);
    }

    @GetMapping("/message-entities/{message}")
    public ResponseEntity<Message> messageResponseEntity(@PathVariable(name = "message") String message) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(new Message(message));
    }

    public static class Message {
        private final String value;

        public Message(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

    }
}
