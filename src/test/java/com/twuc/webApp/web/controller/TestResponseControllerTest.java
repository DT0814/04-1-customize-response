package com.twuc.webApp.web.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.MimeTypeUtils;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
public class TestResponseControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void test_void_response() throws Exception {
        mockMvc.perform(get("/api/no-return-value"))
                .andExpect(status().isOk())
                .andExpect(content().string(""));
    }

    @Test
    void test_response_status_204() throws Exception {
        mockMvc.perform(get("/api/no-return-value-with-annotation"))
                .andExpect(status().isNoContent())
                .andExpect(content().string(""));
    }

    @Test
    void test_response_return_string() throws Exception {
        mockMvc.perform(get("/api/messages/good"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MimeTypeUtils.TEXT_PLAIN_VALUE + ";charset=UTF-8"))
                .andExpect(content().string("good"));
    }

    @Test
    void test_response_return_object() throws Exception {
        mockMvc.perform(get("/api/messages-objects/good"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.value").value("good"));
    }

    //2.5 直接返回对象 + @ResponseStatus 标记
    //请创建另一个 API GET /api/message-objects-with-annotation/{message} 和 2.4 一样返回一个 Message 类型的实例并且其 value 为 {message}。
    //请使用 @ResponseStatus 标记将 Status Code 自定义为 202。
    @Test
    void test_response_status_object() throws Exception {
        mockMvc.perform(get("/api/message-objects-with-annotation/good"))
                .andExpect(status().isAccepted())
                .andExpect(jsonPath("$.value").value("good"));

    }

    //2.3 使用 ResponseEntity
    //请创建另一个 API GET /api/message-entities/{message}，其 Response 定义如下：
    //项目
    //说明
    //Status Code
    //200
    //Content-Type
    //application/json
    //Body
    //{ "value": "{message}" }
    //除此之外，还需要包含一个名为 X-Auth 的 header，其值为 me。请书写测试验证以上的 Response
    @Test
    void test_response_entity() throws Exception {
        mockMvc.perform(get("/api/message-entities/good"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.value").value("good"));

    }
}
